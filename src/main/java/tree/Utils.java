package tree;

import java.io.File;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to store loose methods that have no other place then in a
 * class that has only one method, because a class with one method is not a class.
 *
 * @author Nicolas.
 */
public class Utils {
	/**
	 * This method will recursivly go through all the nodes and construct a JTree
	 * node tree to be used to display all the nodes within a JTree.
	 *
	 * @param node the first root node in a tree of org.w3c.dom.Node 's.
	 * @return The full JTree compatible tree of tree nodes.
	 * @see javax.swing.JTree
	 * @see org.w3c.dom.Node
	 */
	public static DefaultMutableTreeNode iterateNode(Node node) {
		DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(node.getNodeName());
		if (node.hasAttributes()) {
			NamedNodeMap attr = node.getAttributes();
			for (int i = 0; i < attr.getLength(); i++) {
				Node attribute = attr.item(i);
				treeNode.add(new DefaultMutableTreeNode(attribute.getNodeName() + " = " + attribute.getNodeValue()));
			}
		}

		if (node.hasChildNodes()) {
			NodeList children = node.getChildNodes();
			
			for (int i = 0; i < children.getLength(); i++) {
				Node child = children.item(i);

				if (child.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element)child;
					treeNode.add(iterateNode(element));
				}

				if (child.getNodeType() == Node.TEXT_NODE && !child.getNodeValue().trim().equals("")) {
					treeNode.add(new DefaultMutableTreeNode(child.getNodeValue()));
				}
			}
		}

		return treeNode;
	}

	/**
	 * This method will read an xml file and return a Document object containing
	 * the parsed DOM of the xml file.
	 *
	 * @param file The source xml file to be read.
	 * @return The document containing the parsed DOM of the xml file.
	 */
	public static Document readXml(File file) {
		DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			DocumentBuilder docBuild = docFac.newDocumentBuilder();
			doc = docBuild.parse(file);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return doc;
	}
}
