package window;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tree.Utils;

/**
 * Window is a wrapper class that handles most of the displaying and gui manipulation
 * of the GUI.
 *
 * @author Nicolas.
 */
public class Window {
	/**
	 * frame is the code of the GUI and is used to display everything to the user.
	 */
	private JFrame frame;

	/**
	 * This is the main menu bar used in the frame to give the user all options.
	 */
	private JMenuBar menuBar;

	/**
	 * The main view of the gui, it allows the entire xml tree to be shown.
	 */
	private JScrollPane view;

	/**
	 * This is the default constructor for the window/frame, it handles building
	 * the frame and setting everything up to load and show xml trees.
	 */
	public Window() {
		this.frame = new JFrame("xml reader");
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setSize(640, 480);
		this.menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		JMenuItem openMenuItem = new JMenuItem("Open");
		JMenuItem closeMenuItem = new JMenuItem("Close File");
		openMenuItem.addActionListener(ev -> Window.this.openFile());
		closeMenuItem.addActionListener(ev -> Window.this.closeFile());

		fileMenu.add(openMenuItem);
		fileMenu.add(closeMenuItem);

		this.menuBar.add(fileMenu);
		this.frame.setJMenuBar(this.menuBar);
		this.frame.setLayout(new BorderLayout());

		this.view = new JScrollPane();
		this.frame.add(this.view, BorderLayout.CENTER);

		this.show();
	}

	/**
	 * This method is responsible for opening an xml file and loading it into the
	 * JTree tree using a file chooser dialog.
	 */
	public void openFile() {
		File selectedFile = null;

		JFileChooser fileChooser = new JFileChooser();

		int result = fileChooser.showOpenDialog(null);

		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();

			Node root = Utils.readXml(selectedFile).getFirstChild();
			root.normalize();

			JScrollPane pane = new JScrollPane(new JTree(Utils.iterateNode(root)));
			this.frame.remove(this.view);
			this.frame.add(this.view = pane, BorderLayout.CENTER);

			this.frame.revalidate();
			this.frame.repaint();
		}
	}

	/**
	 * This method handles closing a file and clearing everything inside the gui
	 * to prepare for the next opening of a file.
	 */
	public void closeFile() {
		this.frame.remove(this.view);
		JScrollPane pane = new JScrollPane();
		this.frame.add(this.view = pane, BorderLayout.CENTER);

		this.frame.revalidate();
		this.frame.repaint();
	}

	/**
	 * This method makes the gui visible.
	 */
	public void show() {
		this.frame.setVisible(true);
	}
}

